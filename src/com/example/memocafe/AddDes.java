package com.example.memocafe;

import android.os.Bundle;   
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

public class AddDes extends Activity {

	long id;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.addnew_activity);
		
		Intent data = this.getIntent();
		if (data.hasExtra("id")){
			EditText edName = (EditText)findViewById(R.id.edName);
			edName.setText(data.getStringExtra("name"));
			EditText edPhone = (EditText)findViewById(R.id.edTel);
			edPhone.setText(data.getStringExtra("phone"));
			EditText edLocate = (EditText)findViewById(R.id.edlocate);
			edLocate.setText(data.getStringExtra("locate"));
			id = data.getLongExtra("id", 0);
			
			//Toast t = Toast.makeText(this, "Selected ID = "+String.valueOf(id), Toast.LENGTH_LONG);
			//t.show();
			
			
		} 
		
	}

	

	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.add_new, menu);
		return true;
	}
	
	
	
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
		case R.id.action_save:
			EditText edName = (EditText)findViewById(R.id.edName);
			EditText edPhone = (EditText)findViewById(R.id.edTel);
			EditText edLocate = (EditText)findViewById(R.id.edlocate);
		
			
			String sName = edName.getText().toString().trim();
			String sPhone = edPhone.getText().toString().trim();
			String sLocate= edLocate.getText().toString().trim();
			
			
			if (sName.length() == 0 /*|| sPhone.length() == 0 || sLocate.length() == 0*/) {
				Toast t = Toast.makeText(this, 
						"Name, Number phone, and Location are required", 
						Toast.LENGTH_LONG);
				t.show();
			}
			else {
				Intent data = new Intent();
				data.putExtra("name", sName);
				data.putExtra("phone", sPhone);
				data.putExtra("locate", sLocate);
				data.putExtra("pos", id);
				
				
				setResult(RESULT_OK, data);
				finish();
			}
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}

