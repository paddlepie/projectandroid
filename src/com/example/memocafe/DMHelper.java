package com.example.memocafe;


import android.content.Context;    
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


// this is a class that manage the structure of data table
public class DMHelper extends SQLiteOpenHelper {
	
	private static final String DBNAME = "memocafe.db";
	private static final int DBVERSION = 1;
	
	public DMHelper(Context ctx) {
		super(ctx, DBNAME, null, DBVERSION);//dbversion help to control database
	}

	@Override//called when the application is newly installed
	//no database file in the internal storage
	public void onCreate(SQLiteDatabase db) {
		//the primary key of the table need to be "_id"
		//when we want to use this table with ListView
		String sql = "CREATE TABLE food (" +
				"_id integer primary key autoincrement, " +
				"food_name text not null, " +
				"food_phone text not null, " +
				"food_locate text not null);";
		db.execSQL(sql);
		
		String sql1 = "CREATE TABLE dessert (" +
				"_id integer primary key autoincrement, " +
				"dessert_name text not null, " +
				"dessert_phone text not null, " +
				"dessert_locate text not null);";
		db.execSQL(sql1);
		
		String sql2 = "CREATE TABLE pub (" +
				"_id integer primary key autoincrement, " +
				"pub_name text not null, " +
				"pub_phone text not null, " +
				"pub_locate text not null);";
		db.execSQL(sql2);
	}

	@Override//called when the database file exists,
	//but the DBVERSION was increased
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		//Upgrade by removing the current table and recreate a new one
		//this is Not a practical way
		//you should use ALTER TABLE when you need to change the structure of the table
		String sql = "DROP TABLE IF EXISTS contacts;";
		String sql1 = "DROP TABLE IF EXISTS contacts;";
		String sql2 = "DROP TABLE IF EXISTS contacts;";
		db.execSQL(sql);
		db.execSQL(sql1);
		db.execSQL(sql2);
		this.onCreate(db);
	}

}
