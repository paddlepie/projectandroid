package com.example.memocafe;





import android.app.Activity;      
import android.app.ListActivity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;
import android.widget.AdapterView.AdapterContextMenuInfo;

public class dessert extends ListActivity {
	
	DMHelper dmHelper;
	SQLiteDatabase db;
	Cursor cursor;//manage the retrieved records from the database
	SimpleCursorAdapter adapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.food_activity);
		Intent i = this.getIntent();
		
	
		
		dmHelper = new DMHelper(this);
		db = dmHelper.getWritableDatabase();
		cursor = getAllDes();
		adapter = new SimpleCursorAdapter(this, R.layout.item, cursor, 
				new String[] {"dessert_name", "dessert_phone", "dessert_locate"},
				new int[] {R.id.tvname, R.id.tvtel, R.id.tvlocate}, 0);
		setListAdapter(adapter);
		registerForContextMenu(getListView());
	}
	
	private Cursor getAllDes() {
		//db.query = execute a SELECT statement and return a cursor
		return db.query("dessert", //table name
				new String[] {"_id", "dessert_name", "dessert_phone", "dessert_locate"}, //list of column to be retrieved
				null, //conditions for WHERE clause, "ct_name LIKE ?"
				null, //values for the conditions, new String[] {"John%"}
				null, //GROUP BY
				null, //HAVING
				"dessert_name asc");//ORDER BY
	
		}
	
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);//getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		getMenuInflater().inflate(R.menu.context, menu);
	}
	
	public void onBackPressed() {
		super.onBackPressed();
		cursor.close();
		db.close();
		dmHelper.close();
		finish();
	}
	
	



	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		if (requestCode == 9999 && resultCode == RESULT_OK) {
			ContentValues v = new ContentValues();
			v.put("dessert_name", data.getStringExtra("name"));
			v.put("dessert_phone", data.getStringExtra("phone"));
			v.put("dessert_locate", data.getStringExtra("locate"));
			
			db.insert("dessert", null, v);//SQL INSERT statement
			
			//refresh the ListView by(1) re-retrieving the record,(2) set the new cursor to the adapter
			//(3) calling notifyDataSetChanged to update the ListView
			cursor = getAllDes();
			adapter.changeCursor(cursor);
			adapter.notifyDataSetChanged();
		}if (requestCode == 8888 && resultCode == RESULT_OK) {
			ContentValues v = new ContentValues();
			v.put("dessert_name", data.getStringExtra("name"));
			v.put("dessert_phone", data.getStringExtra("phone"));
			v.put("dessert_locate", data.getStringExtra("locate"));
			long pos = data.getLongExtra("pos", -1);
			
			String selection = "_id = ?";
			String[] selectionArgs = {String.valueOf(pos)};
			db.update("dessert", v, selection, selectionArgs);//SQL INSERT statement
			
			
			//Toast t = Toast.makeText(this, "Selected ID = "+String.valueOf(pos), Toast.LENGTH_LONG);
			//t.show();
			//refresh the ListView by(1) re-retrieving the record,(2) set the new cursor to the adapter
			//(3) calling notifyDataSetChanged to update the ListView
			cursor = getAllDes();
			adapter.changeCursor(cursor);
			adapter.notifyDataSetChanged();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
		case R.id.action_new:
			Intent i = new Intent(this, AddNew.class);
			startActivityForResult(i, 9999);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo a = (AdapterContextMenuInfo)item.getMenuInfo();
		long id = a.id;//column _id of the table
		int position = a.position;
		
		switch(item.getItemId()) {
		case R.id.action_edit:
			Cursor c = (Cursor)adapter.getItem(position);//get the record at the position
			String name = c.getString(c.getColumnIndex("dessert_name"));//get the value of the column "ct_name"
			String phone = c.getString(c.getColumnIndex("dessert_phone")); 
			String locate = c.getString(c.getColumnIndex("dessert_locate")); 
			
			//create an intent to send data to AddNewActivity
			Intent i = new Intent(this, AddNew.class);
			i.putExtra("name", name);
			i.putExtra("phone", phone);
			i.putExtra("locate", locate);
			
			i.putExtra("id", id);//int
			startActivityForResult(i,8888);
			//Toast t = Toast.makeText(this, "Selected ID = "+id+
			//		" with name = "+name, Toast.LENGTH_LONG);
			//t.show();
			return true;
		case R.id.action_delete:
			String selection = "_id = ?";
			String[] selectionArgs = {String.valueOf(id)};
			db.delete("dessert", selection, selectionArgs);
			
			cursor = getAllDes();
			adapter.changeCursor(cursor);
			adapter.notifyDataSetChanged();
			
			return true;
		}
		return super.onContextItemSelected(item);
	}
}